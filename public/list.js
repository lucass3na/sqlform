$(document).ready(function(){
    $( '#modalEdit' ).on( 'click' ,'.btn-enviar', function(e){ 
       event.preventDefault();

          var id=$(".btn-enviar").attr('item-id');    
          var data={"id":id};
          var descripcion=$("#editDescripcion").val();
          console.log(descripcion);
          
          $.ajax({
            url: '/table/list/'+id+'/store',
            type: 'POST',
            dataType: 'JSON',
            data: {'id':id,
            "descripcion":descripcion}
          })
          .done(function(res) {
            console.log(this);
            $("#modalEdit").modal("hide");    
            $('.descripcion').html(res.descripcion);      
            console.log("success");
          })
          .fail(function(res) {
            console.log(res);
          })
          .always(function() {
            console.log("complete");
          });
      } );

    $( '#table' ).on( 'click' , '.btn-edit', function(e){ 
        
          e.preventDefault();
          var id=$(this).attr('item-id');

          $.ajax({
            url: '/table/list/'+id,
            type: "get",
            dataType: 'json'
          })
          .done(function(r) {

            $("#editDescripcion").html(r.descripcion);
            $("#editContenido").html(r.contenido);
            $(".btn-enviar").attr("item-id",id);
            $("#modalEdit").modal("show");  

          })
          .fail(function(r) {

            console.log("error");   
          }); 
      });
    $( '#modalEdit' ).on( 'click' , '.cambiarEdicion', function(e){ 
          $('.editDescripcion').toggle();

          if ($('.cambiarEdicion').html()=='Editar Contenido') {
            $('.cambiarEdicion').html('Editar Descripcion');
          }
          else{
            $('.cambiarEdicion').html('Editar Contenido');
          }

          $('.editContenido').toggle();
    })

    $( '#table' ).on( 'click' , '.filas', function(e){ 
        
          e.preventDefault();

          var id=$(this).parent().attr('id');
          console.log(this);
          $.ajax({
            url: '/table/list/'+id,
            type: "get",
            dataType: 'json'
          })
          .done(function(r) {
            
             $("#viewContenido").html(r.contenido);
             $('#viewContenido').attr('readonly',"readonly")
             $("#modalView").modal("show"); 
            
          })
          .fail(function(r) {
            console.log("error");   
          }); 
      });
  
    $( '#table' ).on( 'click' , '.btn-delete', function(){ 
        
          event.preventDefault();
          var id=$(this).attr("item-id");
          var elemento=$('#'+id);
          $.ajax({
            url: '/table/destroy/'+id,
            type: 'POST',
            dataType: 'JSON',
            data: {"id":id},
          })
          .done(function() {

            elemento.remove();
            console.log("success");
          })
          .fail(function() {
            console.log("error");
          })
          .always(function() {
            console.log("complete");
          });    
    });  

  $("#searchInput").focus();
    $("#searchInput").keyup( function(event) {
      event.preventDefault();
        var q = $("#searchInput").val();

        $.ajax({
          url: '/table/search/',
          type: 'GET',
          dataType: 'JSON',
          data: {'q': q },
        })
        .done(function(response) {

            if ($("#searchInput").val()=="") 
              {
                
              }
              else
              {
                var template = _.template(document.getElementById('script').innerHTML);
                var querys = template({response});
                var filas = document.getElementById('tbody').innerHTML = querys;
              }
          console.log("success");               
          })
            .fail(function(response) {
          console.log(response);
          })
            .always(function() {
          console.log("complete");
          });
      });


    $('.groupDiv').on('click', '.groupName', function(event) {
      var name=$(this).attr('id');
      $('.groupInput').attr('value',name);
      console.log($('.groupInput').val());
      
    });
  

});



    

  
