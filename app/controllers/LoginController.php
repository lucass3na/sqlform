<?php

class LoginController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(!Auth::check()){
			return View::make('login.login');
		}
		else{
			return Redirect::to('/table/list');
		}
	}

public function login()
	{
			
			$email = Input::get('email');
			$password = Input::get('password');

			$validator = Validator::make(
	    	array('email' => $email,
	    		'password' => $password),

	    	array('email' => 'required|email|min:2',
	    		'password'=> 'required|min:3')
			);
			
			if ($validator->fails()) {
				return Redirect::to('login');
			}
			else{
				if (!Auth::attempt(array('email' => $email, 'password' => $password))){	
					return Redirect::to('login');
				}
				else{
					return Redirect::to('/table/list');
				}
			}
		
	}


	public function logout(){
		if(Auth::check()){
			Auth::logout();
			return Redirect::to('login');
		}
		else{
			return Redirect::to('login');
		}
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 $groups = ::all();
	 [ 
			'groups' => $groups
		]
	 * @return Response
	 */
	public function create()
	{
		if(!Auth::check()){
			$groups = Group::all();
			return View::make('login.register',['groups' => $groups]);
		}
		else{
			return Redirect::to('/table/list');
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		try {
			$user = new User();
			$user->name = Input::get('name');
			$group= Group::where('name','like',Input::get('group_Name'))->first();
			$user->group_id =$group->id;
			$user->email = Input::get('email');
			$user->password=Hash::make(Input::get("password"));//Hash::make('password_a_encriptar');
			$user->save();
			
			Auth::login($user);
			return Redirect::to('/table/list');		
		}
		catch (Exception $e) {
			echo($e->getMessage());
			die;
			return Redirect::to('/register');
		}
	}


	
	


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
		return View::make("/hello");
	}





}
