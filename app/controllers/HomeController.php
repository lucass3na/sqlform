<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index(){
		$user = User::all();

        return View::make('hello', ['user' => $user]);
	}

	public function showWelcome()
	{
		$groups = Group::all();

		return View::make('hello',['groups' => $groups]);
	}

	public function store()
	{
		header("Content-Type: application/json;charset=utf-8");

		$group = new Group();
		$group->name = Input::get('name');

		$json = json_encode($group);
			if ($json === false) {
			    // Avoid echo of empty string (which is invalid JSON), and
			    // JSONify the error message instead:
			    $json = json_encode(array("jsonError", json_last_error_msg()));
			    if ($json === false) {
			        // This should not happen, but we go all the way now:
			        $json = '{"jsonError": "unknown"}';
			    }
			    // Set HTTP response status code to: 500 - Internal Server Error
			    http_response_code(500);
			}
		$json->save();

		return json_encode($json);
	}

}
