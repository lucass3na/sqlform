<?php

class GroupController extends \BaseController {

	public function group()
	{
		$groups=Group::all();
		
		return View::make('group.group',["groups" => $groups]);
	}


	public function groupStore()
	{
		$group = new Group();
		$group->name = Input::get('name');
		$group->save();
		
		return json_encode($group);
	}

	public function groupDestroy($id)
	{
		$group = Group::find( $id );
		$group->delete();

		return json_encode($id);
	}

}
