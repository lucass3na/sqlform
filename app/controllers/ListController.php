<?php

class ListController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$querys = Query::all();
		return View::make('table.list', [ 
			'querys' => $querys
		]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$query = Query::find(Input::get('id'));
		$query->descripcion = Input::get('descripcion');
		$query->contenido = Input::get('contenido');
		$query->save();
		return json_encode($query);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$query = Query::find($id);
		return json_encode($query);
	}

	public function search()
	{
		$param=Input::get();
		$param=Implode($param);

		$querys = Query::join('user','query.usuario_id','=','user.id')	
				->whereNull('query.deleted_at')
				->Where('user.name','like',$param.'%')
				->where('user.id','=',Auth::id())
				->orwhere(function($q) use ($param){
	            	$q->whereNull('query.deleted_at')
	            	->where('user.email','like',$param.'%')
	            	->where('user.id','=',Auth::id());
	            })
				->orwhere(function($q) use ($param){
	            	$q->whereNull('query.deleted_at')
	            	->where('query.descripcion','like',$param.'%')
	            	->where('user.id','=',Auth::id());
	            })
	            ->orWhere(function($q) use ($param){
	            	$q->whereNull('query.deleted_at')
	            	->where('query.id','=',$param)
	            	->where('user.id','=',Auth::id());
	            })
				->select('query.id','query.usuario_id as usuario_id','query.contenido','query.descripcion','user.name','user.email','query.group_id')
				->get();

		
		return json_encode($querys);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}



	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$query = Query::find( $id );
		$query->delete();
		return json_encode($id);
	}


}
