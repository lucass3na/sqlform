
@extends('layout')
@section('head')
<link rel="stylesheet" type="text/css" href="/list.css">
@stop
@section('navBar')
<div class='form-group divSearch'>
  <input id='searchInput'type="text" class="form-control" placeholder="Search">
</div>
@stop

@section('content')
<div class="areaForm">          
  <table id="table" class=" table ">    
      <thead >     
            <th>id</th>
            <th>dueño</th>
            <th>description</th>
            <th>email</th>
            <th>grupo</th>
            <th>Edit</th>
            <th>Delete</th>
      </thead>
      <tbody id="tbody">
          @foreach($querys as $query)
                @if(Auth::user()->id==$query->usuario_id)                            
                <tr id="{{$query->id}}" >             
                  <td class='filas id'>{{$query->id}}</td>
                  <td class='filas name'>{{$query->user->name}}</td>
                  <div><td id='descrip'class="descripcion filas">{{$query->descripcion}}</td></div>
                  <td class='filas email'>{{$query->user->email}}</td>
                  <td class='filas group'>{{$query->group_id}}</td>
                  <td>
                    <p data-placement="top" data-toggle="tooltip" title="Edit">
                      <button class="btn btn-primary btn-xs btn-edit" item-id="{{$query->id}}" type="submit" >
                        <span class="glyphicon glyphicon-pencil"></span>
                      </button>
                    </p>
                  </td>
                  <td>
                    <p data-placement="top" data-toggle="tooltip" title="Delete">       
                      <button class="btn btn-danger btn-xs btn-delete" item-id="{{$query->id}}" type="submit" >
                        <span class="glyphicon glyphicon-trash"></span>
                      </button>
                    </p>
                  </td>
                </tr>
                @endif 
          @endforeach 
      </tbody>  
  </table>                          
</div>
  <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Editar Query</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 editDescripcion"> <textarea name="editDescripcion" id="editDescripcion"></textarea></div>
          </div>
          <div class="row">
            <div class="col-md-12 editContenido"> <textarea name="editContenido" id="editContenido"></textarea></div>
          </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-default cambiarEdicion">Editar Contenido</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button class="btn-enviar" type="button" class="btn btn-primary">Guardar Cambios</button>
          </div>
        </div>
      </div>
    </div>
  </div>


<div class="modal fade" id="modalView" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Contenido Query</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12"> <textarea name="viewContenido" id="viewContenido"></textarea></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
</div>

@stop
  <script type="text/template" id="script">
  <%_.each(response,function(query){%>
    
            <tr id="<%=query.id%>">
              <td class='filas id'><%=query.id %></td>
              <td class='filas name'><%=query.name %></td>
              <td class="descripcion filas"><%= query.descripcion %></td>
              <td class='filas email'><%= query.email %></td>
              <td class='filas group'><%= query.group_id %></td>
              <td>
               <p data-placement="top" data-toggle="tooltip" title="Edit">
                  <button class="btn btn-primary btn-xs btn-edit" item-id="<%=query.id%>" type="submit" >
                    <span class="glyphicon glyphicon-pencil"></span>
                  </button>
                </p>
              </td>
              <td>
                <p data-placement="top" data-toggle="tooltip" title="Delete">       
                  <button class="btn btn-danger btn-xs btn-delete" item-id="<%=query.id%>" type="submit">
                    <span class="glyphicon glyphicon-trash"></span>
                  </button>
                </p>
              </td>
            </tr>
            <% }); %>

  </script>


