@extends('layout')

@section('content')
	  
<form  class='area' action="/table/create" method="post" role="form">
	<br style="clear:both"/>
  <h3 style="margin-bottom: 25px; text-align: center;">Form Query</h3>
  <div class="form-group">
    <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="descripcion" required>
  </div>
  <div class="form-group">
		<textarea class="form-control" name="contenido" id="contenido" placeholder="contenido" maxlength="200" rows="7"></textarea>
  </div>
  <div class="control-group">
		<div class="controls btn-form">
		  <button class="btn btn-success">enviar</button>
		</div>
  </div>
</form>
		
@stop