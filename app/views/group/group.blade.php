@extends('layout')
@section('head')
  <style>
  	.row{
  			 margin-right:40% ;
        margin-left:30% ;
        margin-top: 10%;	
  	}
    .creados{
      margin-top: 15%;
    }
  </style>
@stop

@section('content')
  <div id='groupsId'>
    <div>
      <legend style=' padding-left:15%;padding-bottom: 0%;padding-top: 0% 'class="">Grupo nuevo</legend>
    </div>
    <div class="control-group">   
      <label class="control-label" name="name">Nombre</label>
        <div class="controls">
          <input type="text" name="name" id="nameInput" placeholder="" class="input-xlarge">
        </div>
    </div>
    <div class="control-group">
      <div class="controls">
       <button class="btn btn-success">Crear</button>
      </div>
    </div>
    <div class="control-group creados">
      <label class="control-label">Grupos creados</label>
      <div class="selectContainer">
        <ul>           
          @foreach($groups as $group)
            <li item-id="{{$group->id}}">
            {{$group->name}}
              <button class="btn-delete">
                <span class="glyphicon glyphicon-trash"></span>
              </button>
            </li>
          @endforeach
         </ul>
      </div>
    </div>
  </div>
  @stop
  @section('footer')
  <script type="text/javascript">
    $(document).ready(function(){
          // EVENTO NEW
          $(".btn-success").click(function(e){
              e.preventDefault();
              var nombre=$("#nameInput").val();
              var data={"name":nombre};

              $.ajax({
                  url: '/group',
                  type: "POST",
                  dataType: 'json',
                  data: data,
              })
              .done(function(response) {
                  $(".selectContainer ul").append( '<li item-id = "'+response.id+'" >'+ response.name +'<button class="btn-delete"><span class="glyphicon glyphicon-trash"></span></button> </li>');
                  $("#nameInput").val("");
              })
              .fail(function(resp) {
                  console.log("error");
              });
          });
          // EVENTO DELETE
          $(document).on('click', '.btn-delete', function(e) {
              e.preventDefault();
              console.log($(".btn-delete"));
          
              var li=$(this).parent();
              var parent=li.attr("item-id");
              $.ajax({
                  type:"POST",
                  url:'/group/destroy/'+parent,
                  data:parent,
                  success:function(){
                      $(li).remove();
                  }
              });
          });
      });
  </script>
@stop




              