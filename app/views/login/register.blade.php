@extends('layoutGuest')

@section('content')
<div class='area'>
  <form class="form-horizontal" action='/register' method="POST">
    <div id="legend">
      <legend class="">Registrar</legend>
    </div>
      <div class="control-group">   
          <label class="control-label" name="name">Nombre</label>
          <div class="controls">
              <input type="text" name="name" placeholder="" class="input-xlarge">
          </div>
      </div>
    <div class="control-group">
      <label class="control-label">Grupo</label>
      <div class="selectContainer groupDiv">
        <div class="controls">
            <input type="text" name="group_Name" id="nameInput" class="input-xlarge groupInput">
        </div>
          <fieldset>
            @foreach($groups as $group)
              <input type="radio" class='groupName' id='{{$group->name}}' value="{{$group->id}}" name='groupSelect'>
              <label> {{$group->name}}</label><br>  
            @endforeach
          </fieldset>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label"  for="email">Email</label>
        <div class="controls">
          <input type="text" id="email" name="email" placeholder="" class="input-xlarge">
        </div>
    </div>
    <div class="control-group">
        <!-- Password-->
      <label class="control-label" for="password">Password</label>
        <div class="controls">
          <input type="password" id="password" name="password" placeholder="" class="input-xlarge">
        </div>
    </div>
    <div class="control-group">
        <!-- Button -->
      <div class="controls">
        <button class="btn btn-success">Registrar</button>
      </div>
    </div>
  </form>
</div>
@stop




              