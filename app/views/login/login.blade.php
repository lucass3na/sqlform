@extends('layoutGuest')

@section('content')
<div class='area'>
  <form class="form-horizontal" action="/login" method="post" name="Login_Form">
    <fieldset>
      <div id="legend">
        <legend class="">Login</legend>
      </div>
      <div class="control-group">    
        <label class="control-label"  for="email">email</label>
  	      <div class="controls">
  	        <input type="text" id="email" name="email" placeholder="" class="input-xlarge">
  	      </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="password">Password</label>
  	      <div class="controls">
  	        <input type="password" id="password" name="password" placeholder="" class="input-xlarge">
  	      </div>
      </div>
      <div class="control-group">
        <div class="controls">
          <button class="btn btn-success" name="Submit" value="Login" type="Submit">Register</button>
        </div>
      </div>
    </fieldset>
  </form>
</div>
@stop