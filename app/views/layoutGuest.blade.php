<!doctype html>
<html lang="en">
  <head>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    
   <link rel="stylesheet" type="text/css" href="/layout.css">
    <script src="http://underscorejs.org/underscore.js"></script>
    <script type="text/javascript" src="/list.js"></script>
    
    
    @yield('head')
    <title>@yield('title')</title>
  </head>
  <body>
    <header>
      <div>
        <div id='divNombre'>
          <a class="navbar-brand" href="/login">SQL Query</a>          
          @yield('navBar')
        </div>  
        <div id='divButtons'>
          <ul id='paddingUl' class="nav navbar-nav">
            <li class="loginButton">
              <a href="/login">Login</a>
            </li>
            <li class="registerButton">
              <a href="/register">Register</a>
            </li>
          </ul>     
        </div>  
      </div> 
    </header>
    <div id='divBody'>
      @yield('content')
    </div>
    <footer></footer>
  </body>
</html>