<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'ListController@index');

Route::group( array( 'before' => 'auth' ), function(){
	Route::get('/table/list', 'ListController@index');
	Route::get('/table/search/','ListController@search');
	Route::get('/table/create', 'CreateController@create');
	Route::post('/table/create', 'CreateController@store');

	Route::get('/logout', 'LoginController@logout');

	Route::get('/group', 'GroupController@group');
	Route::post('/group','GroupController@groupStore');
	Route::post('/group/destroy/{id}','GroupController@groupDestroy');

	Route::get('/table/list/{id}', 'ListController@show');
	Route::post('/table/list/{id}/store','ListController@store');
	Route::post('/table/destroy/{id}', 'ListController@destroy');
});
Route::get('/login', 'LoginController@index');
Route::post('/login', 'LoginController@login');
Route::get('/register', 'LoginController@create');
Route::post('/register', 'LoginController@store');

Route::get('/group', 'GroupController@group');
Route::post('/group','GroupController@groupStore');
