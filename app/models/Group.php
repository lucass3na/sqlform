<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
class Group extends Eloquent {

	use SoftDeletingTrait;

	protected $table = 'group';
	protected $fillable = array(
		'id', 'name');

    protected $dates = ['deleted_at'];
    
	public function user(){
		return $this->has_many('user', 'usuario_id', 'id' );
	}



}