<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
class Query extends Eloquent {

	use SoftDeletingTrait;

	protected $table = 'query';
	protected $fillable = array(
		'id','contenido', 'descripcion', 'usuario_id');

    protected $dates = ['deleted_at'];

		

	public function user(){
		return $this->belongsTo('user', 'usuario_id', 'id' );
	}



}
